#!/bin/bash
set -e

[ -e ketida-server ] || git clone git@gitlab.coko.foundation:ketida/server.git
[ -e ketida-vanilla-client ] || git clone git@gitlab.coko.foundation:ketida/vanilla-client.git
[ -e ketida-client ] || git clone git@gitlab.coko.foundation:ketida/ketida.git ketida-client